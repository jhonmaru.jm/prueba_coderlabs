<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Inicio</title>
  @include('layout.head')
</head>
@include('layout.nav')
<body>
  <style type="text/css">
    .w-5{
      width: 50px;
    }
  </style>
  <div class="container-fluid" style="padding: 5%;">
    <h4 class="text-center">PRODUCTOS</h4>
    <br>
    @if(session('msj_success'))
      <div class="alert alert-success">
        <span>{{ session('msj_success') }}</span>
      </div>
    @endif
    <div class="row">
      @foreach($products as $product)
        <div class="card" style="width: 18rem;margin: 0px 10px 0px 10px; box-shadow: 2px 3px 7px -2px #917628;">
          <img src="{!! asset($product->img_product) !!}" class="card-img-top" alt="..." width="90" height="200">
          <div class="card-body">
            <h5 class="card-title">{!! $product->name !!}</h5>
            <p class="card-text">{!! $product->description !!}</p>
            @if(Cart::content()->where('id', $product->id)->count() == 0)
              <form action="{!! url('carrito/adicionar') !!}" method="POST"> 
                @csrf
                <div class="input-group mb-3">
                  <input name="product_id" value="{!! $product->id !!}" type="hidden">
                  <input name="cant" type="number" class="form-control" min="1" value="1">
                  <button class="input-group-text btn btn-primary" type="submit" id="basic-addon1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart-plus-fill" viewBox="0 0 16 16">
                      <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 
                        .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM9 5.5V7h1.5a.5.5 0 0 1 0 
                        1H9v1.5a.5.5 0 0 1-1 0V8H6.5a.5.5 0 0 1 0-1H8V5.5a.5.5 0 0 1 1 0z"/>
                    </svg> Añadir Carrito
                  </button>
                </div>
              </form>
            @endIf
            @foreach(Cart::content() as $cart) 
              @if($cart->id == $product->id)
                @if(($product->amount - $cart->qty) > 0)
                  <form action="{!! url('carrito/adicionar') !!}" method="POST"> 
                    @csrf
                    <div class="input-group mb-3">
                      <input name="product_id" value="{!! $product->id !!}" type="hidden">
                      <input name="cant" type="number" class="form-control" min="1" value="1">
                      <button class="input-group-text btn btn-primary" type="submit" id="basic-addon1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart-plus-fill" viewBox="0 0 16 16">
                          <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 
                            .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM9 5.5V7h1.5a.5.5 0 0 1 0 
                            1H9v1.5a.5.5 0 0 1-1 0V8H6.5a.5.5 0 0 1 0-1H8V5.5a.5.5 0 0 1 1 0z"/>
                        </svg> Añadir Carrito
                      </button>
                    </div>
                  </form>
                @else
                  <span class="alert alert-danger"> Agotado</span> 
                @endIf
              @endIf
            @endForeach
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item text-center"><strong>PRECIO:</strong> ${!! $product->price !!}</li>
          </ul>
        </div>
      @endForeach
    </div>
  </div>
</body>
</html>
 

