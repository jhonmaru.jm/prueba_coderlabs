<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registrar Pedido</title>
  @include('layout.head')
</head>
<body>
@include('layout.nav')
<div class="container-md" style="margin-top: 2%;">
    <div class="card">
      <div class="card-header" style="background: #005e56;color: white;">
        <h4>MI PEDIDO</h4>
      </div>
      <div class="card-body row">
        <br>
        @if(session('msj_success'))
          <div class="alert alert-success">
            <span>{{ session('msj_success') }}</span>
          </div>
        @endif
        @if(session('msj-error'))
          <div class="alert alert-danger">
            <p>{{ session('msj_error') }}</p>
          </div>
        @endif
        <div class="col-md-4" style="margin:auto; text-align: center">
          <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-bag-fill" viewBox="0 0 16 16">
            <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5z"/>
          </svg>
          <br><br>
        </div>
        <div class="col-md-8">
          <form class="row g-3" action="{!! url('pedido/registrar') !!}" method="POST">
            @csrf
            <div class="col-md-12">
              <input type="text" class="form-control" value="{!! Auth::User()->name !!} {!! Auth::User()->last_name !!}" disabled>
            </div>
            <div class="col-md-12">
              <input type="number" class="form-control" value="{!! Auth::User()->cedula !!}" disabled>
            </div>
            <div class="col-md-12">
              <input name="direction" type="text" class="form-control" value="{!! old('direction') !!}" placeholder="Direccion">
              @if($errors->has('direction'))
                <div class="badge text-bg-danger">{!! $errors->first('direction') !!}</div>
              @endIf
            </div>
            <ul class="list-group">
              <li class="list-group-item active" aria-current="true">Mis productos</li>
              @foreach(Cart::content() as $cart) 
                <li class="list-group-item">
                  <label>{!! $cart->name !!} {!! $cart->qty !!}</label>
                </li>
              @endForeach
            </ul>
            <br>
            <div class="col-md-12" style="text-align: center;">
              @if(Cart::content()->count() >= 1)
                <button type="submit" class="btn btn-success" style="font-size: 17px;">Continuar</button>
              @else 
                <span class="alert alert-danger">Sin productos Registrados</span>
              @endIf
            </div>
          </form>
        </div>
        <br>
        <div class="container-fluid" style="padding: 5%;">  
          <h4 class="text-center">MIS PEDIDOS</h4>
          <table class="table">
            <thead class="text-center">
              <tr>
                <th scope="col">Nombres y Apellidos</th>
                <th scope="col">Direccion</th>
                <th scope="col">Fecha</th>
                <th scope="col">Productos</th>
                <th scope="col">Total Pagar</th>
                <th scope="col">Estado</th>
                <th scope="col">Opciones</th>
              </tr>
            </thead>
            <tbody class="table-group-divider">
              @foreach($orders as $order)
                <tr>
                  <th scope="row">{!! $order->name !!} {!! $order->last_name !!}</th>
                  <td>{!! $order->direction !!}</td>
                  <td>
                    {!! $order->created !!}
                  </td>
                  <td>
                    @foreach($products as $product)
                      @if($product->id_orders == $order->id)
                        <li>{!! $product->name_product !!} ({!! $product->cant !!})</li>
                      @endIf
                    @endForeach
                  </td>  
                  <td>
                    ${!! $order->total!!}
                  </td>
                  <td class="text-center">
                    @if($order->estado == 1)
                      <span class="badge rounded-pill text-bg-warning">PENDIENTE</span>
                    @else
                      <span class="badge rounded-pill text-bg-success">PAGADO</span>
                    @endIf
                  </td>
                  <td>
                    <div class="btn btn-success">
                      <a class="dropdown-item" href="{!! url('pedido/pago') !!}/{!! $order->id !!}">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-credit-card-fill" viewBox="0 0 16 16">
                        <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v1H0V4zm0 3v5a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7H0zm3 2h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1a1 1 0 0 1 1-1z"/>
                      </svg> Pagar
                      </a>
                    </div>
                    <div class="btn btn-danger">
                      <form action="{!! url('pedido/eliminar') !!}/{!! $order->id !!}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="dropdown-item" type="submit">
                          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                          </svg>
                        </button>
                      </form>
                    </div>
                  </td>
                </tr>
              @endForeach
            </tbody>
          </table> 
        </div>
      </div>
    </div>
  </div>
</body>
</html>
