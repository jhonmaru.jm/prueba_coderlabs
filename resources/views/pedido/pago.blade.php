<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registrar Pedido</title>
  @include('layout.head')
</head>
<body>
@include('layout.nav')
  <div class="container-md" style="margin-top: 2%;">
    <br>
    <div class="card mb-3" style="max-width: 540px; margin:auto">
      <div class="row g-0">
        <div class="col-md-4" style="margin:auto;text-align:center">
          <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-credit-card-fill" viewBox="0 0 16 16">
            <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v1H0V4zm0 3v5a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7H0zm3 2h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1a1 1 0 0 1 1-1z"/>
          </svg>
        </div>
        <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title">CANCELACION DEL PEDIDO N°{!! $id !!}</h5>
            <p class="card-text">Por favor introduce tu cedula y contraseña para validar el pago.</p>
            @if(session('msj_success'))
              <div class="alert alert-success">
                <span>{{ session('msj_success') }}</span>
              </div>
            @endif
            @if(session('msj_error'))
              <div class="alert alert-danger">
                <span>{{ session('msj_error') }}</span>
              </div>
            @endif
            <form method="post" action="{!! url('pedido/verificacion') !!}/{!! $id !!}" method="POST">
              <br>
              @csrf
              <div class="mb-3">
                <input class="form-control" type="cedula" name="cedula" id="cedula" placeholder="Cedula" required>
              </div>
              <div class="mb-3">
                <input class="form-control" type="password" name="password" id="password" placeholder="Password" required>
              </div>
              <button class="btn btn-success col-12" tipe="submit">CONTINUAR</button>
              <br>
              <br>
            </form>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
