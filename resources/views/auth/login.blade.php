<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
  @include('layout.head')
</head>
<body>
  <div class="card" style="width: 22rem; margin: 10% auto; box-shadow: 7px 6px 7px -3px #adb5bd;">
    @if(session('msj'))
      <div class="alert alert-danger">
        {{ session('msj') }}
      </div>
    @endif
    <div class="card-body" >
      <form method="post">
        <h5 class="text-center">INICIAR SESIÓN</h5>
        <br>
        @csrf
        <div class="mb-3">
          <label for="email" class="form-label">CEDULA</label>
          <input class="form-control" type="cedula" name="cedula" id="cedula" required>
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">PASSWORD</label>
          <input class="form-control" type="password" name="password" id="password" required>
        </div>
        <button class="btn btn-success col-12" tipe="submit">Login</button>
        <br>
        <a href="{!! url('/inicio') !!}" s>Ir a Inicio</a>
        <br>
      </form>
    </div>
  </div>
</body>
</html>