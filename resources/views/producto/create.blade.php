<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registrar Producto</title>
  @include('layout.head')
</head>
<body>
@include('layout.nav')
<div class="container-md" style="margin-top: 2%;">
    <div class="card">
      <div class="card-header" style="background: #005e56;color: white;">
        <h4>REGISTRO PRODUCTOS</h4>
      </div>
      <div class="card-body row">
        <br>
        @if(session('msj_success'))
          <div class="alert alert-success">
            <span>{{ session('msj_success') }}</span>
          </div>
        @endif
        @if(session('msj-error'))
          <div class="alert alert-danger">
            <p>{{ session('msj_error') }}</p>
          </div>
        @endif
        <div class="col-md-4" style="margin:auto; text-align: center">
          <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-box-seam-fill" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M15.528 2.973a.75.75 0 0 1 .472.696v8.662a.75.75 0 0 1-.472.696l-7.25 2.9a.75.75 0 0 1-.557 0l-7.25-2.9A.75.75 0 0 1 0 12.331V3.669a.75.75 0 0 1 .471-.696L7.443.184l.01-.003.268-.108a.75.75 0 0 1 .558 0l.269.108.01.003 6.97 2.789ZM10.404 2 4.25 4.461 1.846 3.5 1 3.839v.4l6.5 2.6v7.922l.5.2.5-.2V6.84l6.5-2.6v-.4l-.846-.339L8 5.961 5.596 5l6.154-2.461L10.404 2Z"/>
          </svg>
          <br><br>
        </div>
        <div class="col-md-8">
          <form class="row g-3" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="col-md-12">
              <input name="name" type="text" class="form-control" value="{!! old('name') !!}" placeholder="Nombre">
              @if($errors->has('name'))
                <div class="badge text-bg-danger">{!! $errors->first('name') !!}</div>
              @endIf
            </div>
            <div class="col-md-12">
              <input name="amount" type="number" class="form-control" min="0" value="{!! old('amount') !!}" placeholder="Cantidad">
              @if($errors->has('amount'))
                <div class="badge text-bg-danger">{!! $errors->first('amount') !!}</div>
              @endIf
            </div>
            <div class="col-md-12">
              <input name="price" type="number" class="form-control" min="0" value="{!! old('price') !!}" placeholder="Precio">
              @if($errors->has('price'))
                <div class="badge text-bg-danger">{!! $errors->first('price') !!}</div>
              @endIf
            </div>
            <div class="col-md-12">
              <input name="brand" type="text" class="form-control" value="{!! old('brand') !!}" placeholder="Marca">
              @if($errors->has('brand'))
                <div class="badge text-bg-danger">{!! $errors->first('brand') !!}</div>
              @endIf
            </div>
            <div class="col-md-12">
              <textarea name="description" class="form-control" rows="3" placeholder="Describa brevemente el producto.">{{ old('description') }}</textarea>
              @if($errors->has('description'))
                <div class="badge text-bg-danger">{!! $errors->first('description') !!}</div>
              @endIf
            </div>
            <div class="input-group mb-3 col-md-12">
              <label class="input-group-text" for="img_product" style="background: #258497;color: white;">Imagen Producto</label>
              <input name="img_product" class="form-control" type="file" value="{!! old('img_product') !!}">
              @if($errors->has('img_product'))
                <div class="badge text-bg-danger">{!! $errors->first('img_product') !!}</div>
              @endIf
            </div>
            <div class="col-md-12" style="text-align: center;">
              <button type="submit" class="btn btn-primary" style="font-size: 17px;">REGISTRAR</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
