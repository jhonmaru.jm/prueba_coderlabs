<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>productos</title>
  @include('layout.head')
</head>
@include('layout.nav')
<body>
  <style type="text/css">
    .w-5{
      width: 50px;
    }
  </style>
  <div class="container-fluid" style="padding: 5%;">  
    <h4 class="text-center">PRODUCTOS</h4>
    <div class="text-end" width="100%">
      <nav class="navbar navbar-light float-right">
        <form class="d-flex" role="search">
          <select name="tipo" class="form-control mr-sm-2" id="exampleFormControlSelect1">
            <option value="name">nombre</option>
            <option value="price">precio</option>
            <option value="brand">marca</option>  
          </select>
          <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Escribe algo..." aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
        </form>
        <a type="button" class="btn btn-outline-primary" href="{!! url('productos/registrar') !!}">Registrar</a>
      </nav>
    </div>
    <table class="table">
      <thead class="text-center">
        <tr>
          <th scope="col">Nombre</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Estado</th>
          <th scope="col">Precio</th>
          <th scope="col">Marca</th>
          <th scope="col">Descripcion</th>
          <th scope="col">Imagen</th>
          <th scope="col">Opciones</th>
        </tr>
      </thead>
      <tbody class="table-group-divider">
        @foreach($products as $product)
          <tr>
            <th scope="row">{!! $product->name !!}</th>
            <td>{!! $product->amount !!}</td>
            <td>
              @if($product->estado == 1)
                <span class="badge text-bg-success" style="font-size: 14px">EXISTENTE</span>
              @else
                <span class="badge text-bg-danger" style="font-size: 14px">SIN EXISTENCIA</span>
              @endIf
            </td>
            <td>{!! $product->price !!}</td>
            <td>{!! $product->brand !!}</td>
            <td>
              @if($product->description == null)
                Sin Descripción
              @else  
                {!! $product->description !!}
              @endIf
            </td>
            <td class="text-center">
              @if($product->img_product == null)
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" color="grey" fill="currentColor" class="bi bi-image" viewBox="0 0 16 16">
                  <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
                  <path d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z"/>
                </svg>
              @else  
                <img src="{!! asset($product->img_product) !!}" width="100" height="70">
              @endIf
            </td>
            <td>
              <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-gear-fill" viewBox="0 0 16 16">
                  <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
                </svg>
              </button>
              <ul class="dropdown-menu">
                <li>
                  <a class="dropdown-item" href="{!! url('productos/editar') !!}/{!! $product->id !!}">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                  </svg> Editar
                  </a>
                </li>
                <li>
                  <form action="{!! url('productos/eliminar') !!}/{!! $product->id !!}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="dropdown-item" type="submit">
                      <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16" color="red">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                      </svg> Eliminar
                    </button>
                  </form>
                </li>
              </ul>
            </td>
          </tr>
        @endForeach
      </tbody>
    </table> 
  </div>
</body>
</html>


