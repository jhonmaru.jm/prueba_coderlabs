<nav class="navbar navbar-expand-lg bg-body-tertiary" style="background: #ffc107; box-shadow: 5px 3px 8px 2px grey;">
  <div class="container-fluid">
    <a class="navbar-brand" href="#" style="width:40%">CODERLABS</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{!! url('/inicio') !!}"><strong>INICIO</strong></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{!! url('/productos') !!}"><strong>PRODUCTOS</strong></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{!! url('pedido/index')!!}"><strong>MIS PEDIDOS</strong></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <button type="button" class="btn btn-primary position-relative">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
                <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 
                5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 
                1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
              </svg>
              <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                TOTAL ({!! Cart::content()->count() !!})
                <span class="visually-hidden">unread messages</span>
              </span>
            </button>
          </a>
          <ul class="dropdown-menu" style="width:200px; padding:10%">
            @foreach(Cart::content() as $cart) 
              <li class="list-group-item" style="background: #efebeb;padding: 3%;">
                <form action="{!! url('carrito/eliminar') !!}" method="POST" width="30"> 
                  @csrf
                    <label style="width:120px">{!! $cart->name !!} ({!! $cart->qty !!})</label>
                    <input name="cart_id" value="{!! $cart->rowId !!}" type="hidden">
                    <button class="input-group-text btn btn-danger" type="submit" style="margin:1%">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                      </svg>
                    </button>
                </form>
              </li>
            @endForeach
            @if( Cart::content()->count() >= 1 )
              <li style="padding: 2%;text-align: center;background: #19446e;color: white">Total: ${!! Cart::priceTotal(0) !!}</li>
              <li class="list-group-item">
                <a href="{!! url('pedido/index') !!}" width="30">
                  <button class="input-group-text btn btn-success" type="submit" style="margin:1%; width:100%">Procesar
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cash-coin" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M11 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm5-4a5 5 0 1 1-10 0 5 5 0 0 1 10 0z"/>
                    <path d="M9.438 11.944c.047.596.518 1.06 1.363 1.116v.44h.375v-.443c.875-.061 1.386-.529 1.386-1.207 0-.618-.39-.936-1.09-1.1l-.296-.07v-1.2c.376.043.614.248.671.532h.658c-.047-.575-.54-1.024-1.329-1.073V8.5h-.375v.45c-.747.073-1.255.522-1.255 1.158 0 .562.378.92 1.007 1.066l.248.061v1.272c-.384-.058-.639-.27-.696-.563h-.668zm1.36-1.354c-.369-.085-.569-.26-.569-.522 0-.294.216-.514.572-.578v1.1h-.003zm.432.746c.449.104.655.272.655.569 0 .339-.257.571-.709.614v-1.195l.054.012z"/>
                    <path d="M1 0a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4.083c.058-.344.145-.678.258-1H3a2 2 0 0 0-2-2V3a2 2 0 0 0 2-2h10a2 2 0 0 0 2 2v3.528c.38.34.717.728 1 1.154V1a1 1 0 0 0-1-1H1z"/>
                    <path d="M9.998 5.083 10 5a2 2 0 1 0-3.132 1.65 5.982 5.982 0 0 1 3.13-1.567z"/>
                  </svg>
                  </button>
                </a>
              </li>
            @else
              <li><span>Carrito Vacio</span></li>
            @endIf
          </ul>
        </li>
      </ul>
      @if (Auth::guest())
        <a class="btn btn-outline-primary" href="{!! url('/login')!!}">Login</a>
        <a class="btn btn-outline-primary" href="{!! url('/registro')!!}">Registrarme</a>
      @else
        <form class="d-flex" action="/logout" method="post">
          @csrf
          <a class="btn btn-outline-success" type="submit" href="#" onclick="this.closest('form').submit()">Salir</a>
        </form>
      @endif
    </div>
  </div>
</nav>
