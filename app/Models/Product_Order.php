<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_Order extends Model
{
    use HasFactory;

    /*
    * Autor: Jhon Marulada
    * Descripcion: Modelo para enlazar productos con pedidos.
    */
    protected $table = 'products_orders';
    protected $filliable = [
        'id_orders', 'id_products', 'estado', 'cant'
    ];

    public $timestamps = false;
}
