<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /*
    * Autor: Jhon Marulada
    * Descripcion: Modelo para registar productos.
    */
    protected $table = 'products';
    protected $filliable = [
        'name', 'amount', 'price', 'brand', 'description', 'img_product', 'estado', 'created'
    ];
    
    public $timestamps = false;

    public function scopeBuscarpor($query, $tipo, $buscar) {

    	if ( ($tipo) && ($buscar) ) {
    		return $query->where($tipo,'like',"%$buscar%");
    	}
    }
}
