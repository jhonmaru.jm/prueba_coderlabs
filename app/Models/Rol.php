<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    use HasFactory;

    /*
    * Autor: Jhon Marulada
    * Descripcion: Modelo para registar roles a un usuario.
    */
    protected $table = 'roles';
    protected $filliable = [
        'cedula', 'tipo', 'created'
    ];
    
    public $timestamps = false;
}
