<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    /*
    * Autor: Jhon Marulada
    * Descripcion: Modelo para registar pedidos.
    */
    protected $table = 'orders';
    protected $filliable = [
        'id_users', 'direction', 'total', 'estado', 'create'
    ];
    
    public $timestamps = false;
}
