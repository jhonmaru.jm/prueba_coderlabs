<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\Rol;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Administrador
{
    public function handle(Request $request, Closure $next)
    {   
        $data = Rol::where('cedula', Auth::user()->cedula)->first();
        //Si es igual a 1 es usuario Administrador
        if($data->tipo == 1){
            return $next($request);
        }
        return abort(403);
    }
}
