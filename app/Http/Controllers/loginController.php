<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
  /*
   * Autor: Jhon Marulada
   * Descripción: Modulo de logueo utilizando el estandar OAuth
   */
  
  //Retornamos vista login.blade
  public function index(Request $request){
    return view('auth.login');
  }

  //Validacion de credenciales 'Cedula' y 'Password'
  public function login(Request $request){

    $credenciales = request()->only('cedula', 'password');

    if(Auth::attempt($credenciales)){

      request()->session()->regenerate();
      return redirect('inicio/');
    }
    return back()->with('msj', 'Credenciales Incorrectas');
  }

  //Cierre de Sesión 
  public function logout(Request $request){
    
    Auth::logout();
    
    $request->session()->invalidate(); 
    $request->session()->regenerateToken();

    return redirect('/login');
  }

}
