<?php

namespace App\Http\Controllers;

use DateTime;

use App\Models\Product;
use App\Services\PayUService\Exception;

use Illuminate\Http\Request;
date_default_timezone_set('America/Bogota');

class productoController extends Controller
{
  /*
  * Autor: Jhon Marulada
  * Descripcion: CRUD modulo Productos.
  */

  //Retorna la vista con los datos de los productos.
  public function index(Request $request){

    $buscar = $request->get('buscarpor');
    $tipo = $request->get('tipo');
    $products = Product::buscarpor($tipo, $buscar)->paginate(5);


    //$products = Products::select('id', 'name', 'amount', 'estado', 'price', 'brand', 'description', 'img_product', 'estado')->get();

    return view('producto.index', compact('products'));
  }

  //Retorna la vista del formulario registro de productos.
  public function create(){
    return view('producto.create');
  }  

  //Validaciones y guardado de un producto en la BD.
  public function store(Request $request){
    $datatime   = new DateTime();
    $fecha_hora = $datatime->format('Y-m-d'); 

    try {
      $datos = request()->validate([
        'name'        => 'required|max:25|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/',
        'amount'      => 'required|numeric|max:999',
        'price'       => 'required|numeric',
        'brand'       => 'required|max:25',
        'description' => 'nullable|max:200',
        'img_product' => 'nullable|mimes:jpg,png',
      ],[
        'required'        => 'Este campo es obligatorio.',
        'numeric'         => 'Solo se admiten números.',
        'name.max'        => 'Este campo no puede superar los 25 caracteres.',
        'name.regex'      => 'Solo se permiten letras.',
        'amount.max'      => 'La cantidad no pude ser mayor o igual a 1000.',
        'price.max'       => 'Este campo no puede superar los 25 caracteres.',
        'description.max' => 'Este campo no puede superar los 200 caracteres.',
        'mimes'           => 'Solo se permite formato JPG o PNG.',
      ]);
    
      $producto = new Product;
  
      $producto->name        = $request->name;
      $producto->amount      = $request->amount;
      $producto->price       = $request->price;
      $producto->brand       = $request->brand;
      $producto->description = $request->description;
      $imagen                = $request->file('img_product');
      if(isset($imagen)){
        $file_name             = $fecha_hora.'_'.$imagen->getClientOriginalName();
        $producto->img_product = '../img_product/'.$file_name;
      }else{
        $producto->img_product = null;
      }
      $producto->estado      = 1;
      $producto->created     = $fecha_hora; 
  
      if($producto->save() == true){
        if(isset($imagen)){
          $imagen->move(public_path().'/img_product/', $file_name);
        }
        return back()->with('msj_success', 'El Producto se creo exitosamente.');
      } 
      return back()->with('msj_error', 'Error al crear Producto.');
    }catch (ValidationException $e) { 
      $this->assertSame($exception, $e); 
    }
  }

  //Retorna la vista para editar un producto.
  public function edit($id){

    $product = Product::select('id', 'name', 'amount', 'estado', 'price', 'brand', 'description', 'img_product', 'estado')
      ->where('id', $id)
      ->get();

    return view('producto.edit', compact('product'));
  }

  //Función para actulizar el producto.
  public function update(Request $request, $id){
    $datatime   = new DateTime();
    $fecha_hora = $datatime->format('Y-m-d'); 

    try {
      $datos = request()->validate([
        'name'        => 'required|max:25|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/',
        'amount'      => 'required|numeric|max:999',
        'price'       => 'required|numeric',
        'brand'       => 'required|max:25',
        'description' => 'nullable|max:200',
        'img_product' => 'nullable|mimes:jpg,png',
      ],[
        'required'        => 'Este campo es obligatorio.',
        'numeric'         => 'Solo se admiten números.',
        'name.max'        => 'Este campo no puede superar los 25 caracteres.',
        'name.regex'      => 'Solo se permiten letras.',
        'amount.max'      => 'La cantidad no pude ser mayor o igual a 1000.',
        'price.max'       => 'Este campo no puede superar los 25 caracteres.',
        'description.max' => 'Este campo no puede superar los 200 caracteres.',
        'mimes'           => 'Solo se permite formato JPG o PNG.',
      ]);

      $imagen = $request->file('img_product');

      //Prodecimiento en caso tal de que la imagen del producto sea actulizada
      if(isset($imagen)){
        $file_name = $fecha_hora.'_'.$imagen->getClientOriginalName();
        $ruta_img  = '../img_product/'.$file_name;
        $product = Product::where('id', $id)
        ->update(['img_product' => $ruta_img]);
        $imagen->move(public_path().'/img_product/', $file_name);
      }

      $product = Product::where('id', $id)
        ->update(['name'        => $request->name, 
                  'amount'      => $request->amount, 
                  'price'       => $request->price, 
                  'brand'       => $request->brand,
                  'description' => $request->description,
                ]);
      return back()->with('msj_success', 'El Producto se edito de manera correcta.');
    }catch (ValidationException $e) { 
      $this->assertSame($exception, $e); 
    }
  }

  //Función para eliminar un producto.
  public function destroy($id){

    $product = Product::find($id);
    $product->delete();

    return back()->with('msj', 'El libro se elimino correctamente');
  }


}
