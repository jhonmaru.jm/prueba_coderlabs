<?php

namespace App\Http\Controllers;

use App\Models\Product;

use Illuminate\Http\Request;

use Gloudemans\Shoppingcart\Facades\Cart;

class carritoController extends Controller
{
  /*
  * Autor: Jhon Marulada
  * Descripcion: Modulo Carrito de compras.
  */

  //Función para diccionar productos.
  public function adicionar(Request $request){

    $product = Product::findOrFail($request->input('product_id'));
    $cart    = Cart::content();

    Cart::add($product->id,
              $product->name,
              $request->input('cant'),
              $product->price          
            );
    return redirect()->route('inicio')->with('msj_success', 'Producto '.$product->name.' agregado con exito.');
  }

  //Funciona para eliminar productos.
  public function destroy(Request $request){
    
    $cart = Cart::remove($request->cart_id);

    return redirect()->route('inicio')->with('msj_success', 'Se elimino producto del carrito de compra.');
  }
}
