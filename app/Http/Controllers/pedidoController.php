<?php

namespace App\Http\Controllers;

use Auth;
use DateTime;

use App\Models\Order;
use App\Models\Product_Order;

use Illuminate\Http\Request;

use Gloudemans\Shoppingcart\Facades\Cart;
date_default_timezone_set('America/Bogota');

class pedidoController extends Controller
{
  public function index(Request $request){
    
    $cart = Cart::content();
    $orders = Order::selectRaw('id, (SELECT name FROM users WHERE users.id = id_users) AS name, 
      (SELECT last_name FROM users WHERE users.id = id_users) AS last_name, direction, created, total, estado')
      ->where('id_users', Auth::User()->id)
      ->get(); 
    $products = Product_Order::join('orders', 'orders.id', '=', 'products_orders.id_orders')
      ->selectRaw('id_orders, id_products, (SELECT name FROM products WHERE products.id = id_products) AS name_product, cant, orders.id_users')
      ->where('orders.id_users', Auth::User()->id)
      ->get();

    return view('pedido.index', compact('cart', 'orders', 'products'));
  }

  public function store(Request $request){

    $datatime   = new DateTime();
    $fecha_hora = $datatime->format('Y-m-d'); 

    try {
      $datos = request()->validate([
        'direction'   => 'required'
      ],[
        'required'        => 'Este campo es obligatorio.'
      ]);
    
      $order = new Order;
  
      $order->id_users       = Auth::User()->id;
      $order->direction      = $request->direction;
      $order->total          = str_replace(',','',Cart::priceTotal(0));
      $order->estado         = 1;
      $order->created        = $fecha_hora;
      
      if($order->save()){
        $carts = Cart::content();
        foreach($carts as $cart){
          $product_order = new Product_Order;

          $product_order->id_orders   = $order->id;
          $product_order->id_products = $cart->id;
          $product_order->cant        = $cart->qty;
          $product_order->estado      = 1;
          $product_order->save();
        }
      }
      Cart::destroy();

      return back()->with('msj_success', 'Se registro tu pedido con exito.');
    }catch (ValidationException $e) { 
      $this->assertSame($exception, $e); 
    }
  }

  public function pago($id){

    return view('pedido.pago', compact('id'));
  }

  public function verificacion(Request $request, $id){

    $credenciales = request()->only('cedula', 'password');

    if(Auth::attempt($credenciales)){
      $order = Order::where('id', $id)
        ->update(['estado' => 2]);
      return back()->with('msj_success', 'Pago realizado con exito');
    }
    return back()->with('msj_error', 'Credenciales Incorrectas');
  }

  //Función para eliminar un pedido.
  public function destroy($id){

    $order = Order::find($id);
    $order->delete();

    return back()->with('msj_success', 'El Pedido N° '.$id.' se elimino correctamente');
  }
}
