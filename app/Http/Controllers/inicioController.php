<?php

namespace App\Http\Controllers;

use App\Models\Product;

use Illuminate\Http\Request;

use Gloudemans\Shoppingcart\Facades\Cart;

class inicioController extends Controller
{
  /*
  * Autor: Jhon Marulada
  * Descripcion: Modulo inicio index.  
  */

  //Muestra los productos al usuario en la vista inicio, permitiendo agregar al carrito de compra y hacer pedidos.
  public function inicio(){

    $products = Product::select('id', 'name', 'amount', 'estado', 'price', 'brand', 'description', 'img_product', 'estado')
      ->get();
    $cart = Cart::content();

    return view('inicio', compact('products', 'cart'));
  }
}
