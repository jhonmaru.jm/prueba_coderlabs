<?php

namespace App\Http\Controllers;

use DateTime;

use App\Models\User;
use App\Models\Rol;

use App\Services\PayUService\Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
date_default_timezone_set('America/Bogota');

class usuarioController extends Controller
{
  /*
  * Autor: Jhon Marulada
  * Descripcion: Modulo para la gestión de usuarios.
  */

  //Devulve formulario de registro
  public function create(){
    return view('user.registro');
  }


  //Validaciones y guardado de un usuario en la BD.
  public function store(Request $request){
    $datatime   = new DateTime();
    $fecha_hora = $datatime->format('Y-m-d'); 

    try {
      $data = request()->validate([
        'name'      => 'required|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/|max:50',
        'last_name' => 'required|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/|max:50',
        'cedula'    => "required|numeric|max:99999999999|unique:users,cedula,{$request->id}",
        'password'  => 'required|min:8|regex:/[A-Z]/|max:600|confirmed',
        'password_confirmation' => 'required'
      ],[
        'required'           => 'Este campo es obligatorio.',
        'name.regex'         => 'Formato invalido, solo se permiten letras.',
        'name.max'           => 'Solo se permite un maximo de 50 caracteres.',
        'last_name.regex'    => 'Formato invalido, solo se permiten letras.',
        'last_name.max'      => 'Solo se permite un maximo de 50 caracteres.',
        'cedula.max'         => 'Solo se permite un maximo de 11 caracteres.',
        'cedula.unique'      => 'Esta cedula ya exite.',
        'password.min'       => 'Este campo debe tener almenos 8 caracteres.',
        'password.regex'     => 'Este campo debe tener almenos una mayuscula.',
        'password.max'       => 'Solo se permite un maximo de 600 caracteres.',
        'password.confirmed' => 'Las contraseña no coinciden.',
      ]);
  
      $user = new User;
  
      $user->name      = $request->name;
      $user->last_name = $request->last_name;
      $user->cedula    = $request->cedula;
      $user->password  = Hash::make($request->password);
      $user->created   = $fecha_hora;

      if($user->save()){
        $rol = new Rol;
        $rol->cedula  = $request->cedula;
        $rol->tipo    = 2; //el numero '1' es para administradores y el '2' para usuarios comunes.
        $rol->created = $fecha_hora;
      }
     
      return redirect('/registro')->with('msj', 'Perfil creado con exito!');
  
    }catch (ValidationException $e) { 
      $this->assertSame($exception, $e); 
    }
  }
}
