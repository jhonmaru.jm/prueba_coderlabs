<?php

use App\Http\Controllers\loginController;
use App\Http\Controllers\inicioController;
use App\Http\Controllers\productoController;
use App\Http\Controllers\carritoController;
use App\Http\Controllers\pedidoController;
use App\Http\Controllers\usuarioController;

use Illuminate\Support\Facades\Route;
/*
 * Autor: Jhon Marulada
 * Descripcion: Todas las rutas utilizadas por la aplicación.
 */

//Ruta para redirigir a la vista productos
Route::get('/', function (){
  return redirect('inicio');
});

//Rutas de inicio
Route::get('/inicio', [inicioController::class, 'inicio'])->name('inicio');

//Rutas Modulo usuarios.
Route::get('/registro', [usuarioController::class, 'create']);
Route::post('/registro', [usuarioController::class, 'store']);

//Rutas logueo y cierre de sesion 
Route::get('/login', [loginController::class, 'index'])->name('login')->middleware(['guest']);
Route::post('/login', [loginController::class, 'login']);
Route::post('/logout', [loginController::class, 'logout']);

//Grupo de Rutas CRUD Productos
Route::group(['prefix'=>'productos','middleware'=>['auth', 'administrador']],function () {
  Route::get('/', [productoController::class, 'index']);
  Route::get('/registrar', [productoController::class, 'create']);
  Route::post('/registrar', [productoController::class, 'store']);
  Route::get('/editar/{id}', [productoController::class, 'edit']);
  Route::post('/update/{id}', [productoController::class, 'update']);
  Route::delete('/eliminar/{id}', [productoController::class, 'destroy']);
});

//Grupo de Rutas Carrito de Compra
Route::group(['prefix'=>'carrito','middleware'=>['auth']],function () {
  Route::post('/adicionar', [carritoController::class, 'adicionar']);
  Route::post('/eliminar', [carritoController::class, 'destroy']);
});

//Grupo de rutas gestión de un pedido y pago
Route::group(['prefix'=>'pedido','middleware'=>['auth']], function(){
  Route::get('/index', [pedidoController::class, 'index']);
  Route::post('/registrar', [pedidoController::class, 'store']);
  Route::get('/pago/{id}', [pedidoController::class, 'pago']);
  Route::post('/verificacion/{id}', [pedidoController::class, 'verificacion']);
  Route::delete('/eliminar/{id}', [pedidoController::class, 'destroy']);
});
