<?php

use App\Http\Controllers\usuarioController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
 * Autor: Jhon Marulanda
 * Descripción: En este modulo se definen todas las rutas para ser consumidas por Api´s utilizanto Laravel Sanctum.
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/registro', [usuarioController::class, 'create']);