<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    //Tabla de migracion Productos
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name', 25);
            $table->integer('amount');
            $table->integer('price');
            $table->string('brand', 25);
            $table->text('description')->nullable();
            $table->string('img_product', 255)->nullable();
            $table->integer("estado");
            $table->date('created');
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
