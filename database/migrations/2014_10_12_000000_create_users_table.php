<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    //Tabla migracion de Usuarios
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 20);
            $table->string('last_name', 20);
            $table->integer('cedula')->unique();
            $table->string('password');
            $table->date('created');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
