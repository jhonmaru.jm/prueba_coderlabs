<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsOrdersTable extends Migration
{
    
    public function up()
    {
        Schema::create('products_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_orders');
            $table->unsignedBigInteger('id_products');
            $table->foreign('id_products')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('id_orders')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('cant');
            $table->integer('estado');
        });
    }

    public function down()
    {
        Schema::dropIfExists('products_orders');
    }
}
