<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
   
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->integer('cedula')->unique();
            $table->foreign('cedula')->references('cedula')->on('users');
            $table->integer('tipo');
            $table->date('created');
        });
    }

    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
